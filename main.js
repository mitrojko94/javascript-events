window.addEventListener("load", (event) => {
  const joinUp = document.getElementById("joinUp");
  joinUp.classList.add("joinUp");

  const title = document.createElement("h1");
  title.innerText = "Join Our Program";
  joinUp.appendChild(title);

  const paragraph = document.createElement("p");
  paragraph.innerText =
    "Sed do eiusmod tempor incididunt \
     ut labore et dolore magna aliqua.";
  joinUp.appendChild(paragraph);

  const sectionForm = document.createElement("form");
  sectionForm.classList.add("submit");

  const textInput = document.createElement("input");
  textInput.type = "text";
  textInput.placeholder = "Email";
  const textInputClass = ["submit", "input"];
  textInput.classList.add(...textInputClass);

  const submitBtn = document.createElement("button");
  const submitBtnClass = ["submit", "button"];
  submitBtn.innerText = "Subscribe";
  submitBtn.type = "submit";
  submitBtn.id = "subscribe";
  submitBtn.classList.add(...submitBtnClass);
  sectionForm.append(textInput, submitBtn);

  // Add event listener on form section
  sectionForm.addEventListener("submit", (e) => {
    e.preventDefault();
    let enteredValue = e.target[0].value;
    console.log(`Entered value is: ${enteredValue}`);
    enteredValue = "";
  });

  joinUp.append(sectionForm);
});
